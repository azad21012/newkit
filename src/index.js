import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux'
import { AppContainer } from 'react-hot-loader';
import { render } from 'react-dom';

// import { store , history } from './store';
import configureStore, { history } from './store/configureStore';
import App from './App';

const store = configureStore();


ReactDOM.render(
    <Provider store={store} history={history}>
    {/* {console.log('mount DOM')} */}
        <App/>
    </Provider>
    ,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

if (module.hot) {
    module.hot.accept('./App', () => {
        const NewRoot = require('./App').default;
        render(
        <AppContainer>
            <NewRoot store={store} history={history} />
        </AppContainer>,
        document.getElementById('root')
        );
    });
}

