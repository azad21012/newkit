const proxy = require('http-proxy-middleware');
module.exports = function(app) {
    app.use(proxy('/Service01', 
        { target: 'http://localhost:3001/' }
    ));
}