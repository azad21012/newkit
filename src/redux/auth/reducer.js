import { passState } from "../../helpers/passState";
import { fail, success } from "../../helpers/utility";
import actions from "./action";
// console.log(actions) at function invoke many times !!!


export default function authReducer(
    state = { loading: false, data: undefined, error: false },
    action
){
    console.log(action)
    switch (action.type) {
        case actions.LOGIN_REQUEST:
            return { ...state, loading: true, data: undefined, error: false };
        case success(actions.LOGIN_REQUEST):
            return { ...state, loading: false, data: action.response, error: false };
        case fail(actions.LOGIN_REQUEST):
            return {
                ...state,
                loading: false,
                errorMessage: action.errorResponse,
                error: true
            };
        case actions.LOGOUT_REQUEST:
            return { ...state, loading: true };
        case success(actions.LOGOUT_REQUEST):
            return { ...state, data: action.response, loading: false, error: false };
        case fail(actions.LOGOUT_REQUEST):
            return {
                ...state,
                error: true,
                errorMessage: action.errorResponse,
                loading: false
            };
        case actions.CHECK_AUTHORIZATION:
            return { ...state, loading: true };
        case success(actions.CHECK_AUTHORIZATION):
            return { ...state, data: action.response, loading: false, error: false };
        case fail(actions.CHECK_AUTHORIZATION):
            return {
                ...state,
                error: true,
                errorMessage: action.errorResponse,
                loading: false
            };
        default:
            return state;
    }
}

export const AuthSelector = passState(
    {
        isLoggedIn: (state) => state.data !== undefined,
        hasError: (state) => state.error,
        getError: (state) =>
        state.errorMessage ? state.errorMessage.meta.message : "",
        getUser: (state) => state.data,
        loading: (state)=>state.loading
    },
    state => state.Auth
);
