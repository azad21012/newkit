import ApiCall from "../../helpers/ApiCall";

import { success , clearToken} from "../../helpers/utility";
import {history} from '../../store/configureStore'
const actions = {
  CHECK_AUTHORIZATION: "CHECK_AUTHORIZATION",
  LOGIN_REQUEST: "LOGIN",
  LOGOUT_REQUEST: "LOGOUT_REQUEST",
  login: (email, password) =>
    ApiCall({
      type: actions.LOGIN_REQUEST,
      request: {
        method: "POST",
        url: "/Service01/v1/user-login",
        data: { email, password }
      },
      onSuccess: function({ dispatch }, response) {
        dispatch({ type: success(actions.LOGIN_REQUEST), response });
      }
    }),
  checkAuth:()=>
    ApiCall({
      type : actions.CHECK_AUTHORIZATION,
      request:{
        method: "GET",
        url: "/Service01/v1/auth"
      },onSuccess: function({dispatch},response){
        dispatch({type: success(actions.CHECK_AUTHORIZATION),response})
      }
    }),
  logout:()=>
    ApiCall({
      type: actions.LOGOUT_REQUEST,
      request: {
        method: "GET",
        url:"/Service01/v1/logout"
      },
      onSuccess: ({ dispatch },response) => {
        clearToken();
        dispatch({ type: success(actions.LOGOUT_REQUEST),response });
        history.push("/");
      }

    })
};
export default actions;
