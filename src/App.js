import React from "react";
// import { createBrowserHistory } from "history";
import { BrowserRouter } from "react-router-dom";
import './App.scss'
// core components
import "assets/css/material-dashboard-react.css?v=1.6.0";
import Routing from "./routing";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
       {/* {console.log('mount App')}   */}
        <Routing/>
      </BrowserRouter>
    );
  }
}
// Boot().then(()=><App/>).catch(error => console.error(error));
export default App;
