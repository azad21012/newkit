import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import actions from '../../redux/auth/action';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress';

class Login extends React.Component {
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.dispatch(actions.login(this.state.email,this.state.password))
    }
    state={
        email:'',
        password:''
    }
    handleInputEmail = (event)=>{
        this.setState({email:event.target.value})
    }
    handleInputPassword = (event)=>{
        this.setState({password:event.target.value})
    }

    // static getDerivedStateFromProps(nextProps, prevState){
    //     if(nextProps.someValue!==prevState.someValue){
    //     return { someState: nextProps.someValue};
    // }
    // else return null;
    // }
    componentWillReceiveProps(nextProps){
        console.log(nextProps)
    }
  render() {
    return (
      <main className="main">
        <CssBaseline />
        <CircularProgress
          className=""
          variant="determinate"
          value={this.props.auth.loading}
        />
        <Paper className="paper">
          <Avatar className="avatar" style={{backgroundColor:'blue'}}>
            <LockOutlinedIcon  />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className="form" onSubmit={this.handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email Address</InputLabel>
              <Input id="email" name="email" autoComplete="email" autoFocus onChange={this.handleInputEmail} />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input name="password" type="password" id="password" autoComplete="current-password" onChange={this.handleInputPassword} />
            </FormControl>

            <Button
              fullWidth
              variant="contained"
              color="primary"
              className="submit"
              type="submit"
            >
              Sign in
            </Button>
            <Button
            >
            <div className="create">
              <Link to="/register" >
                  Create Account
              </Link>
            </div>
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}
const mapStateToProps = (state) => {
    // eslint-disable-next-line no-console
    console.log(state)
    return {
        auth: state.auth
    }
}
export default connect(mapStateToProps)(Login)


