import React from 'react'
import {connect} from 'react-redux'
import actions from '../redux/auth/action';
const {checkAuth} = actions
export default function(ComposedClass,reload) {
    class AuthenticationClass extends React.Component{
        state={
            loading:true,
        }
        componentWillMount(){
            this.props.dispatch(checkAuth())
        }
        componentWillReceiveProps(nextProps){
            if(nextProps.auth.data){
                this.setState({loading:false})
                if(!nextProps.auth.data.isAuth){
                    if(reload){
                        this.props.history.push('/login')
                    }
                }else{
                    if(reload === false){
                        this.props.history.push('/home')
                    }
                }
            }
        }
        render(){
            if(this.state.loading){
                return(
                    <div className="loader">
                        Loading ...
                    </div>
                )
            }

            return(
                <ComposedClass {...this.props}/>
            )
        }
    }
    const mapStateToProps = (state) => {
        // console.log(state.auth.loading)
        return {
            auth: state.auth
        }
    }
    return connect(mapStateToProps)(AuthenticationClass)

}

