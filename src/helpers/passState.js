export const passState = (selectors, getSelectorState) => {
    return Object.keys(selectors).reduce((prev, current) => {
      prev[current] = state => selectors[current](getSelectorState(state));
      return prev;
    }, {});
};
  