export const success = actionType => `${actionType}_SUCCESS`;
export const fail = actionType => `${actionType}_FAIL`;


const cookie = require('js-cookie')
export const clearToken = () => cookie.remove("auth")
