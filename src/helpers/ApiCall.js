import {
    fail,
    // success,
} from "./utility";
import axios from "axios";
  
  const ApiCall = action => {
    if (typeof action !== "object") {
      throw new Error("action should be object");
    }
  
    if (!action.request) {
      throw new Error("action should have request");
    }
  
    return async (dispatch) => {
      dispatch(action);
      try {
        let response = await axios({
          ...action.request,
        });
        response = response.data;
        console.log(response)
        if (action.onSuccess) {
          return action.onSuccess({ dispatch }, response);
        }
        // return dispatch({
        //   type: success(action.type),
        //   data: response,
        //   meta: {...action}
        // });
      } catch (error) {
        console.log(error)
        return dispatch({
            type: fail(action.type),
            errorCode: error.response.status,
            errorResponse: error.response.data,
            meta: { ...action }
        });
      }
    };
  };
  
  export default ApiCall;
  