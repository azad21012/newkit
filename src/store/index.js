import { createStore, applyMiddleware, compose } from "redux";

import thunk from "redux-thunk";
import reducers from "../redux/reducers";
import promiseMiddleware from 'redux-promise'


import { connectRouter, routerMiddleware } from 'connected-react-router';

import {createBrowserHistory} from 'history';
const history = createBrowserHistory();
const connectRouterHistory = connectRouter(history);
const reactRouterMiddleware = routerMiddleware(history);

const middlewares = [thunk,promiseMiddleware,reactRouterMiddleware];
const store = createStore(
  connectRouterHistory(reducers),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  compose(applyMiddleware(...middlewares))
);
export { store ,history };