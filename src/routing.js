import React from 'react'
import {Switch,Route,Redirect} from 'react-router-dom'


import Admin from "./layouts/Admin";
import RTL from "layouts/RTL.jsx"
import Login from "./containers/Auth/login";
import Auth from './hoc/auth';


class Routing extends React.Component {

    render() {
        // console.log('mount routing') // 1 time rendered
        return (
            <Switch>
                <Route path="/admin" component={Auth(Admin,true)} />
                <Route path="/login" component={Auth(Login,false)} />
                <Route path="/rtl" component={RTL} />
                <Redirect from="/" to="/admin/dashboard" />
            </Switch>
        );
    }

}


export default Routing